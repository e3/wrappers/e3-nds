# e3-nds

Wrapper for Nominal Device Support (NDS).

## Installation

```sh
$ make init patch build
$ make install
```

For further targets, type `make`.

## Usage

```sh
$ iocsh -r "nds"
```

## Contributing

Contributions through pull/merge requests only.
